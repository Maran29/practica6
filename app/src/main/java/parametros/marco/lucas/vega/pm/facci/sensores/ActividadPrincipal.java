package parametros.marco.lucas.vega.pm.facci.sensores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {

    Button Sensores, luz, proximidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);



        Sensores=(Button)findViewById(R.id.btnSensores);
        luz=(Button)findViewById(R.id.btnOrientacion);
        proximidad=(Button)findViewById(R.id.btnProximidad);




        Sensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(ActividadPrincipal.this,ActividadSensorAcelerometro.class);
                startActivity (intent);
            }

        });

        luz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadAceleracion.class);
                startActivity(intent);
            }
        });

        proximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadProximidad.class);
                startActivity(intent);
            }
        });


    }
}

